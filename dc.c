extern char *strdup(const char *s);
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "cola.h"
#include "pila.h"

// TODO
//bool is_number_or_operator(char carac) {
//    char* validos = "123456789+-*/"
//    return strstr(carac, validos);
//}

enum valor_valido {
    NUMERO, OPERADOR
};

bool es_operador(char* c) {
    return strcmp(c, "+") == 0 || strcmp(c, "-") == 0 || strcmp(c, "*") == 0 || strcmp(c, "/") == 0;
}

int operar(char* operador, int num1, int num2) {
    if(strcmp(operador, "+") == 0)
        return num1 + num2;
    else if(strcmp(operador, "-") == 0)
        return num1 - num2;
    else if(strcmp(operador, "*") == 0)
        return num1 * num2;
    else if(strcmp(operador, "/") == 0)
        return num1 / num2;
}

void error(void) {
    printf("La notación para Calculadora Polaca Inversa no se ha realizado correctamente\n");
}

cola_t* string_to_cola(char* cadena) {
    cola_t *cola = cola_crear();
    char carac;
    char constructor[50];
    int contador_constructor = 0;
    int contador = 0;
    char* final;
    do {
        carac = (char)cadena[contador];
        //if(!is_number_or_operator(carac)) return NULL;
        if(carac != ' ' && carac != '\0') {
            constructor[contador_constructor] = carac;
            constructor[contador_constructor + 1] = '\0';
            contador_constructor++;
        }
        else {
            final = strdup(constructor);
            cola_encolar(cola, final);
            contador_constructor = 0;
        }
        contador++;
    } while(carac != '\0');

    return cola;
}

int calculadora_polaca(char* cadena) {
    cola_t *cola = string_to_cola(cadena);
    pila_t *pila = pila_crear();
    int acumulado = 0;
    char* buffer;
    char* total;
    enum valor_valido siguiente = NUMERO;
    int val1, val2;
    char* operador;
    int resultado = 0;

    while(!cola_esta_vacia(cola)) {
        // Si es un operador
        if(es_operador((char*)cola_ver_primero(cola))) {
            buffer = malloc(sizeof(char*) * 50);
            if(pila_esta_vacia(pila)) { error(); free(pila); free(cola); return 0; }
            val1 = atoi((char*)pila_desapilar(pila));
            if(pila_esta_vacia(pila)) { error(); free(pila); free(cola); return 0; }
            val2 = atoi((char*)pila_desapilar(pila));

            operador = cola_desencolar(cola);
            acumulado = operar(operador, val2, val1);
            sprintf(buffer, "%i", acumulado);
            pila_apilar(pila, buffer);
            siguiente = NUMERO;
        }
        else if (!es_operador((char*)cola_ver_primero(cola))) {
            pila_apilar(pila, cola_desencolar(cola));
        }
        else {
            error();
            return 0;
        }
    }
    resultado = atoi((char*)pila_desapilar(pila));
    if(!pila_esta_vacia(pila)) {
        error();
        return 0;
    }
    //printf("%s", constructor);
    cola_destruir(cola, free);
    pila_destruir(pila);

    return resultado;
}

int main(int argc, char* argv[]) {
    int resultado;
    printf("INICIO DE CALCULADORA POLACA\n\n");

    resultado = calculadora_polaca(argv[1]);
    printf("El resultado de tu cuenta es: %i\n", resultado);
    return 0;
}
