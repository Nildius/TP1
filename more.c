    #define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void more(FILE *restrict file, int numero_lineas) {
    size_t capacidad = 0;
    char* linea = NULL;
    ssize_t cant;
    int contador = 0;
    cant = getline(&linea, &capacidad, file);

    while(cant != -1) {
        do {
            printf("%s", linea);
            contador++;
            cant = getline(&linea, &capacidad, file);
        } while(contador < numero_lineas && cant != -1);
        getchar();
        contador = 0;
    }
}

int main(int argc, char* argv[]) {
    if(argc != 3) {
        printf("La cantidad de argumentos es inválida.\n");
        return 1;
    }

    FILE *file = fopen(argv[1], "r");
    if(!file) {
        printf("Ingrese una ruta de archivo valida.\n");
        return 1;
    }
    printf("Leyendo archivo %s, por favor presione Enter para avanzar sobre el archivo", argv[1]);
    getchar();
    more(file, atoi(argv[2]));

    fclose(file);
    return 0;
}
