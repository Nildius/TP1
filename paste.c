#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int paste(FILE *restrict file1, FILE *restrict file2) {
    char* linea1 = NULL;
    char* linea2 = NULL;
    size_t capacidad1 = 0;
    size_t capacidad2 = 0;
    ssize_t cant1;
    ssize_t cant2;
    cant1 = getline(&linea1, &capacidad1, file1);
    cant2 = getline(&linea2, &capacidad2, file2);

    while(cant1 != -1 && cant2 != -1) {
        linea1 = strtok(linea1, "\n");
        linea2 = strtok(linea2, "\n");
        printf("%s \t %s \n", linea1, linea2);

        cant1 = getline(&linea1, &capacidad1, file1);
        cant2 = getline(&linea2, &capacidad2, file2);
    }

    free(linea1);
    free(linea2);
    if(cant1 != -1 || cant2 != -1)
        return -1;
    return 1;
}

int main(int argc, char *argv[]) {
    if(argc != 3) {
        printf("Debes ingresar 2 archivos como parametro.\n");
        return 1;
    }
    FILE *file1 = fopen(argv[1], "r");
    FILE *file2 = fopen(argv[2], "r");
    if(!file1 || !file2) {
        printf("La ruta de los archivos es incorrecta\n");
        return 1;
    }
    paste(file1, file2);
    return 0;
}
