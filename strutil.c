extern char *strdup(const char *s);
#include "strutil.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char** split(const char* str, char sep) {
    int separaciones = 1;
    char c;
    char constructor[80];
    int contador_constructor = 0;
    char** vector;
    int vector_contador = 0;
    char* final;

    // Contamos la cantidad de Separaciones ("palabras") que tendrá el vector
    for(int x = 0; str[x] != '\0'; x++) {
        if(str[x] == sep)
            separaciones++;
    }

    vector = malloc(sizeof(char*) * (separaciones + 1));

    for(int x = 0; str[x] != '\0'; x++) {
        c = str[x];
        if(c != sep) {
            constructor[contador_constructor] = c;
            constructor[contador_constructor + 1] = '\0';
            contador_constructor++;
        }
        else {
            final = strdup(constructor);
            vector[vector_contador] = final;
            vector_contador++;
            contador_constructor = 0;
            constructor[0] = '\0';
        }
    }
    final = strdup(constructor);
    vector[vector_contador] = final;
    vector[vector_contador + 1] = NULL;

    return vector;

}

char* join(char** strv, char sep) {
    int total = 50;
    int carga = 0;
    char* final = malloc(total * sizeof(char));
    char* aux;
    for(int x = 0; strv[x] != NULL; x++) {
        for(int y = 0; strv[x][y] != '\0'; y++) {
            // Si me quedo sin memoria en el Constructor Final del String
            if(carga >= (total - total / 4)) {
                aux = realloc(final, total * 2);
                if(aux)
                    final = aux;
            }

            final[carga] = strv[x][y];
            final[carga + 1] = '\0';
            carga++;
        }
        final[carga] = sep;
        final[carga + 1] = '\0';
        carga++;
    }
    final[carga - 1] = '\0';

    return final;
}

void free_strv(char** strv) {
    for(int x = 0; strv[x] != NULL; x++) {
        free(strv[x]);
    }
    free(strv);
}

int main() {
    char** vector;
    char* uni;
    vector = split("El pato marrano", ' ');

    for(int x = 0; vector[x] != NULL; x++) {
        printf("--> %s\n", vector[x]);
    }


    uni = join(vector, '-');

    printf("%s", uni);
    
    free(uni);
    free_strv(vector);

    return 0;
}
